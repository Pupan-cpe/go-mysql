package usercontrollers

type InputUser struct {
	FullName string `json:"fullname"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type InputLogin struct {
	Password string `json:"password"`
	Email    string `json:"email"`
}