package usercontrollers

import (
	"github.com/labstack/echo/v4"
	"github.com/matthewhartstonge/argon2"
	configs "gitlab.com/Pupan-cpe/go-mysql/config"
	"gitlab.com/Pupan-cpe/go-mysql/models"
)

func GetAll(c echo.Context) error {

	var users []models.User
 	configs.DB.Find(&users)

	return c.JSON(200, map[string]interface{}{

		"Data": users,
		"Meseage":"Get API Success",
	})
}

func Register(c echo.Context)error{

var input InputUser

if err := c.Bind(&input); err != nil{

	return echo.NewHTTPError(400, "err")
}

users := models.User{
	FullName: input.FullName,
	Email: input.Email,
	Password: input.Password,
}

userExists := configs.DB.Where("email = ?", input.Email).Find(&users)

if userExists.RowsAffected ==1{

	return c.JSON(400, map[string]interface{}{
		"error":"duplicate",
	})
}

result := configs.DB.Debug().Create(&users)

if result.Error != nil{

	return c.JSON(400, result.Error)
}else{


	return c.JSON(200, map[string]interface{}{
		"message": "Register Success",


	})
}


}



func Login(c echo.Context)error{

	var input InputLogin

	if err := c.Bind(&input); err != nil{

		return echo.NewHTTPError(400, "err")
	}

	user := models.User{

		Email: input.Email,
		Password: input.Password,
	}


	userAccount := configs.DB.Debug().Where("email = ?", input.Email).First(&user)
	if userAccount.RowsAffected < 1 {

		return c.JSON(400, map[string]interface{}{
			"error": "user account not found",
		})
	}

	ok, _ := argon2.VerifyEncoded([]byte(input.Password),  []byte(user.Password))
		if !ok{
			return c.JSON(400, map[string]interface{}{
				"error": "Password not found",
			})

		}else{

			return c.JSON(200, map[string]interface{}{
				"Message": "Login Success!",
				
			})
		}





	return nil
}