package main

import (
	"fmt"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	configs "gitlab.com/Pupan-cpe/go-mysql/config"
	"gitlab.com/Pupan-cpe/go-mysql/routes"
)

func main() {

	fmt.Println("Hello world")

	router := setupRouter()

	router.Start(":"+os.Getenv("PORT_GO"))
}

func setupRouter()*echo.Echo {


	e := echo.New()

    e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
      AllowOrigins: []string{"*"},
      AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
    }))  

	configs.ConnectDB() //? Configs DB
	home := e.Group("api/v1") //q str group
	routes.InitUserRoutes(home)  //? Router Group
	return e

}