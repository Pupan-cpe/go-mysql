package configs

import (
	"fmt"
	"os"

	"gitlab.com/Pupan-cpe/go-mysql/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)
var DB *gorm.DB

func ConnectDB() {

	envDB := os.Getenv("DATABASE")

	fmt.Println("Connecting", envDB)

	
	db, err := gorm.Open(mysql.Open(envDB), &gorm.Config{})

	if err != nil {

		fmt.Println("ไม่สามารถติดต่อกับฐานข้อมูลได้")

		fmt.Println(err.Error())
		panic(err)

	}else{
		DB = db

		fmt.Println("Connect DB Sucess")

		db.AutoMigrate(&models.User{})




	}
}