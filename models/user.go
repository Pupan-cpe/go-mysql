package models

import (
	"github.com/matthewhartstonge/argon2"
	"gorm.io/gorm"
)

type User struct {
	ID        uint   `json:id gorm:"primaryKey;autoIncrement; not null"`
	FullName  string `json:"full_name" gorm:"type:varchar(255); not null"`
	Email     string `json:"email" gorm:"type:varchar(255); not null"`
	Password  string `json:"-" gorm:"type:varchar(255); not null"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func (user *User) BeforeCreate(db *gorm.DB) error {

	user.Password = hashPassword(user.Password)

	return nil
}

func hashPassword(Password string) string {

	argon := argon2.DefaultConfig()

	encoded, _ := argon.HashEncoded([]byte(Password))

	return string(encoded)
}