# GO Echo with GORM




## Architecture
| Folder | Details |
| --- | ---|
| controllers | Holds the api endpoints |
| config | Database Initializer and DB manager |
| route | router setup |
| model | Models|


## Run 
`go run server.go`

