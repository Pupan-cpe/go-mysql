module gitlab.com/Pupan-cpe/go-mysql

go 1.16

require (
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/labstack/echo/v4 v4.6.1 // indirect
	github.com/matthewhartstonge/argon2 v0.1.5 // indirect
	github.com/tiny-go/middleware v1.0.0 // indirect
	gorm.io/driver/mysql v1.1.3 // indirect
	gorm.io/gorm v1.22.2 // indirect
)
