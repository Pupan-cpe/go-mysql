package routes

import (
	"github.com/labstack/echo/v4"
	userController "gitlab.com/Pupan-cpe/go-mysql/controllers/user"
)

func InitUserRoutes(rg *echo.Group) {
	routerGroup := rg
	routerGroup.GET("/users" , userController.GetAll)
	routerGroup.POST("/users" , userController.Register)
	routerGroup.POST("/users/login" , userController.Login)

}

